const express = require('express');
const boxen = require('boxen');
const app = express();
const port = 5000;

app.listen(port, () =>
  console.log(boxen(`Listening on port ${port}!`, { borderColor: 'green' }))
);

const getRandom = () => {
  return Math.floor(Math.random() * 100 + 1) + '';
};

app.get('/person/:input', (req, res) => {
  res.send({ val1: getRandom(), val2: getRandom() });
});
app.get('/facility/:val1', (req, res) => {
  res.send({ val3: getRandom(), val4: getRandom() });
});
app.get('/exposure/:val2', (req, res) => {
  res.send({ val5: getRandom() });
});
