const callEndPoint = async url => {
  var call = await fetch(url);
  var data = await call.json();
  return data;
};
// I added all calls and calculation to one action.
// It can be separated, if needed.
// I decided to save only result to the global state,
// as all other values are only used in this one function
export default function callEndPoints(input) {
  return async function(dispatch, getState) {
    var data = await callEndPoint(`/person/${input}`);
    var { val1, val2 } = data;
    data = await callEndPoint(`/facility/${val1}`);
    const { val3 } = data;
    data = await callEndPoint(`/exposure/${val2}`);

    var result = Number(val3) * Number(data.val5);
    dispatch({ type: 'SET_RESULT', payload: result });
  };
}
