import React from 'react';
import ReactDOM from 'react-dom';
import './modal.css';

const modalRoot = document.getElementById('modal-root');

class Modal extends React.Component {
  el = document.createElement('div');
  componentDidMount() {
    modalRoot.appendChild(this.el);
  }
  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }
  render() {
    return ReactDOM.createPortal(
      <div
        style={{
          position: 'absolute',
          top: '0',
          bottom: '0',
          left: '0',
          right: '0',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'rgba(0,0,0,0.3)'
        }}
        onClick={this.props.onClose}
      >
        <div
          style={{
            padding: 20,
            background: '#EFEAF2',
            borderRadius: '2px',
            display: 'flex',
            minHeight: '100px',
            margin: '1rem',
            position: 'relative',
            minWidth: '400px',
            boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
            justifySelf: 'center',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          {!this.props.result && <div>Loading...</div>}
          {this.props.result && (
            <p>The result of val3 * val5 is {this.props.result}</p>
          )}
          {this.props.result && <span class='close'>&times;</span>}
        </div>
      </div>,
      this.el
    );
  }
}

export default Modal;
