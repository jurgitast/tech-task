import React, { Component } from 'react';
import { connect } from 'react-redux';
import calcResult from '../actionCreators/calcResult';
import resetResult from '../actionCreators/resetResult';
import './App.css';
import Modal from './Modal';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      isValid: false,
      showResult: false
    };
  }

  handleChange = event => {
    this.setState({ input: event.target.value }, () => {
      this.validateInput();
    });
  };

  validateInput = () => {
    const validInput = /^[a-zA-Z0-9]{1,10}$/;
    this.setState({ isValid: validInput.test(this.state.input) });
  };

  handleClick = async event => {
    event.preventDefault();
    this.props.handleClick(this.state.input);
    this.setState({ showResult: true });
  };

  handleCloseModal = () => {
    this.props.resetResult();
    this.setState({ input: '', showResult: false, isValid: false });
  };

  renderButton = () => {
    const disabledProp = this.state.isValid ? {} : { disabled: true };
    return (
      <button onClick={this.handleClick} {...disabledProp}>
        Submit
      </button>
    );
  };

  render() {
    return (
      <div className='app'>
        <h3>Enter up to 10 symbols and see what happens</h3>
        <div className='form-content'>
          <input
            type='text'
            onChange={this.handleChange}
            value={this.state.input}
          />
          <div className='error'>
            {this.state.isValid ? '' : '* Enter 1 to 10 alphanumeric symbols'}
          </div>
          {this.renderButton()}
          {this.state.showResult ? (
            <Modal onClose={this.handleCloseModal} result={this.props.result} />
          ) : null}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  result: state.result
});

const mapDispatchToProps = dispatch => {
  return {
    handleClick: text => {
      dispatch(calcResult(text));
    },
    resetResult: () => {
      dispatch(resetResult());
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
