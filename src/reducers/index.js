const initialState = {
  result: ''
};
export default function reducer(state = initialState, action) {
  const newState = { ...state };

  switch (action.type) {
    case 'SET_RESULT':
      return { newState, result: action.payload };
    case 'RESET_RESULT':
      return { newState, result: action.payload };
    default:
      return newState;
  }
}
